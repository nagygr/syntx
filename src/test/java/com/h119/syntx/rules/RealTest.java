package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class RealTest {
	@ParameterizedTest
	@ValueSource(
		strings = {
			"-231", "54", "0", "-0", "0.231", "+1953", "1.2", "-23.432",
			"1e-2", "-123.2342e+23", ".23e4"
		}
	)
	void testReal(String number) throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(number));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(number);
	}

	@Test
	void testRealEnding() throws Exception {
		String number = "2.3e3 ";
		var main = rule().called("main").build();
		var context = new Context(fromString(number));

		main.set(real().cat(ws(eps())));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(number);
	}

	@Test
	@SuppressWarnings("java:S5976") // Similar tests should be grouped in a single Parameterized test
	void testRealFailureEmpty() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(""));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a real (decimal mark: '.')."
			);
	}

	@Test
	void testRealFailureJustSign() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("+"));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"+\n" +
				" ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a real (decimal mark: '.')."
			);
	}

	@Test
	void testRealFailureInvalidSign() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("-+"));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"-+\n" +
				" ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a real (decimal mark: '.')."
			);
	}

	@Test
	void testRealFailureInvalidRealPart() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("-e2"));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"-e2\n" +
				" ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a real (decimal mark: '.')."
			);
	}

	@Test
	void testRealFailureEmptyExponentPart() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("-12.23e"));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"-12.23e\n" +
				"       ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a real (decimal mark: '.')."
			);
	}

	@Test
	void testRealFailureInvalidExponentPart() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("-12.23ea"));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"-12.23ea\n" +
				"       ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a real (decimal mark: '.')."
			);
	}

	@Test
	void testRealFailureLeadingZero() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("-0123.32"));

		main.set(real());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo("-0");
	}
}

