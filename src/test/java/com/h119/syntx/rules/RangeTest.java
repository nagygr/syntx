package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class RangeTest {
	@Test
	@SuppressWarnings("java:S5838") // Chained AssertJ assertions should be simplified to the corresponding dedicated assertion
	void testRangeToString() throws Exception {
		Range c = new Range('a', 'c');

		assertThat(c.toString())
			.isEqualTo(
				"a character in the range: ['a', 'c']"
			);
	}

	@Test
	void testRange() throws Exception {
		String input = "b";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(range('a', 'c'));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@Test
	void testRangeFailureWrongCharacter() throws Exception {
		String input = "x";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(range('a', 'c'));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"x\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character in the range: ['a', 'c']."
			);
	}

	@Test
	void testRangeFailureEmptyString() throws Exception {
		String input = "";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(range('a', 'c'));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character in the range: ['a', 'c']."
			);
	}
}

