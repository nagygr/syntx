package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class NonDecimalTest {
	@ParameterizedTest
	@ValueSource(strings = {"-0x231", "0x54a", "0x0", "-0x0", "+0xaab1953"})
	void testNonDecimalHexadecimal(String number) throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString(number));

		main.set(hexadecimal());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(number);
	}

	@ParameterizedTest
	@ValueSource(strings = {"-0b01", "0b11", "0b0", "-0b0", "+0b1011001"})
	void testNonDecimalBinary(String number) throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString(number));

		main.set(binary());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(number);
	}

	@ParameterizedTest
	@ValueSource(strings = {"-bb01", "bb11", "bb0", "-bb0", "+bb1011001"})
	void testNonDecimalBinaryCustomPrefix(String number) throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString(number));

		main.set(binary("bb"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(number);
	}

	@Test
	@SuppressWarnings("java:S5976") // Similar tests should be grouped in a single Parameterized test
	void testNonDecimalHexadecimalFailureNoPrefix() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString("+12"));

		main.set(hexadecimal());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"+12\n" +
				" ^\n" +
				"ERROR: while parsing rule \"int\" the parser expected to find a non-decimal " +
				"(prefix: \"0x\", digits: {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', " +
				"'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F'})."
			);
	}

	@Test
	@SuppressWarnings("java:S5976") // Similar tests should be grouped in a single Parameterized test
	void testNonDecimalHexadecimalFailureWrongPrefix() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString("+012"));

		main.set(hexadecimal());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"+012\n" +
				" ^\n" +
				"ERROR: while parsing rule \"int\" the parser expected to find a non-decimal " +
				"(prefix: \"0x\", digits: {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', " +
				"'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F'})."
			);
	}

	@Test
	@SuppressWarnings("java:S5976") // Similar tests should be grouped in a single Parameterized test
	void testNonDecimalHexadecimalFailureWrongDigits() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString("+0xu12"));

		main.set(hexadecimal());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"+0xu12\n" +
				"   ^\n" +
				"ERROR: while parsing rule \"int\" the parser expected to find a non-decimal " +
				"(prefix: \"0x\", digits: {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', " +
				"'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F'})."
			);
	}

	@Test
	void testNonDecimalHexadecimalFailureWrongDigitsNotFirst() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString("+0x1q2"));

		main.set(hexadecimal());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo("+0x1");
	}

	@Test
	@SuppressWarnings("java:S5976") // Similar tests should be grouped in a single Parameterized test
	void testNonDecimalBinaryFailureWrongLengthEmpty() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString(""));

		main.set(binary());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"int\" the parser expected to find a non-decimal " +
				"(prefix: \"0b\", digits: {'0', '1'})."
			);
	}

	@Test
	void testNonDecimalBinaryFailureWrongLengthAfterSign() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString("+"));

		main.set(binary());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"+\n" +
				" ^\n" +
				"ERROR: while parsing rule \"int\" the parser expected to find a non-decimal " +
				"(prefix: \"0b\", digits: {'0', '1'})."
			);
	}

	@Test
	void testNonDecimalBinaryFailureWrongLengthAfterPrefix() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString("-0b"));

		main.set(binary());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"-0b\n" +
				"   ^\n" +
				"ERROR: while parsing rule \"int\" the parser expected to find a non-decimal " +
				"(prefix: \"0b\", digits: {'0', '1'})."
			);
	}

}

