package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class IdentifierTest {
	@ParameterizedTest
	@ValueSource(strings = {"apple1234", "App1e", "app1e_trEe", "_private_field"})
	void testIdentifier(String identifier) throws Exception {
		var main = rule().called("identifier").build();
		var context = new Context(fromString(identifier));

		main.set(identifier("_"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(identifier);
	}

	@Test
	void testIdentifierPartialMatchDueToAlienCharacter() throws Exception {
		var main = rule().called("identifier").build();
		var context = new Context(fromString("apple_pie"));

		main.set(identifier());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo("apple");
	}

	@Test
	void testIdentifierFailureStartsWithNumber() throws Exception {
		var main = rule().called("identifier").build();
		var context = new Context(fromString("12apples"));

		main.set(identifier("_"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"12apples\n" +
				"^\n" +
				"ERROR: while parsing rule \"identifier\" the parser expected to find an identifier with extra characters: {'_'}."
			);

	}

	@Test
	void testIdentifierFailureEmpty() throws Exception {
		var main = rule().called("identifier").build();
		var context = new Context(fromString(""));

		main.set(identifier());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"identifier\" the parser expected to find an identifier."
			);

	}

}
