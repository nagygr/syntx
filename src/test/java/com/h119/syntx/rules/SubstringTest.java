package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class SubstringTest {
	@Test
	void testSubstring() throws Exception {
		String input = "applepie";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(substring("apple").cat(substring("pie")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@Test
	void testSubstringFailureWrongWord() throws Exception {
		String input = "aplepie";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(substring("apple").cat(substring("pie")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"aplepie\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a substring (\"apple\")."
			);
	}

	@Test
	void testSubstringFailureEmptyString() throws Exception {
		String input = "";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(substring("apple").cat(substring("pie")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a substring (\"apple\")."
			);
	}
}

