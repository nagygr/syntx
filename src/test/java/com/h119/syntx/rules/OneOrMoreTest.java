package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class OneOrMoreTest {
	@ParameterizedTest
	@ValueSource(strings = {"a", "ab", "aabcc"})
	void testOneOrMore(String input) throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(oneOrMore(character("abc")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@Test
	void testOneOrMorePartial() throws Exception {
		String input = "bcd";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(oneOrMore(character("abc")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo("bc");
	}

	@Test
	void testOneOrMoreFailureEmptyString() throws Exception {
		String input = "";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(oneOrMore(character("abc")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character from a set: {'a', 'b', 'c'}."
			);
	}

	@Test
	void testOneOrMoreFailureDifferentContent() throws Exception {
		String input = "d";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(oneOrMore(character("abc")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"d\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character from a set: {'a', 'b', 'c'}."
			);
	}
}

