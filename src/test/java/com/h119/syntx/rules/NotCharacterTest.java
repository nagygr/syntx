package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class NotCharacterTest {
	@Test
	@SuppressWarnings("java:S5838") // Chained AssertJ assertions should be simplified to the corresponding dedicated assertion
	void testCHaracterToString() throws Exception {
		NotCharacter c = new NotCharacter("abc");

		assertThat(c.toString())
			.isEqualTo(
				"a character not from the set: {'a', 'b', 'c'}"
			);
	}

	@Test
	void testNotCharacter() throws Exception {
		String input = "x";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(notCharacter("abc"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@Test
	void testNotCharacterFailureWrongCharacter() throws Exception {
		String input = "b";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(notCharacter("abc"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"b\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character not from the set: {'a', 'b', 'c'}."
			);
	}

	@Test
	void testNotCharacterFailureEmptyString() throws Exception {
		String input = "";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(notCharacter("abc"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character not from the set: {'a', 'b', 'c'}."
			);
	}
}

