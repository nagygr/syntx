package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class WhitespaceTest {
	@ParameterizedTest
	@ValueSource(strings = {" apple", "\t\n  banana", "cherry"})
	void testInteger(String input) throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(ws(identifier()));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input.strip());
	}

	@Test
	void testWhitespaceFailure() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("\t"));

		main.set(ws(integer()));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\t\n" +
				" ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find an integer."
			);
	}
}

