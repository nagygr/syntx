package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class KeywordTest {
	@ParameterizedTest
	@ValueSource(strings = {"the_keyword", "App1e", "app1e_trEe", "_private_field"})
	void testKeyword(String input) throws Exception {
		var main = rule().called("keyword").build();
		var context = new Context(fromString(input));

		main.set(keyword(input));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@Test
	void testKeywordPartialMatchDueToAlienCharacter() throws Exception {
		var main = rule().called("keyword").build();
		var context = new Context(fromString("apple_pie"));

		main.set(keyword("appletart"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"apple_pie\n" +
				"     ^\n" +
				"ERROR: while parsing rule \"keyword\" the parser expected to find a keyword: appletart."
			);
	}

	@Test
	void testKeywordFailureStartsWithNumber() throws Exception {
		var main = rule().called("keyword").build();
		var context = new Context(fromString("12apples"));

		main.set(keyword("apples"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"12apples\n" +
				"^\n" +
				"ERROR: while parsing rule \"keyword\" the parser expected to find a keyword: apples."
			);

	}

	@Test
	void testKeywordFailureEmpty() throws Exception {
		var main = rule().called("keyword").build();
		var context = new Context(fromString(""));

		main.set(keyword("char"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"keyword\" the parser expected to find a keyword: char."
			);
	}

}

