package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class StringLiteralTest {
	@ParameterizedTest
	@ValueSource(strings = {"\"hello world\"", "\"Hello\\t\\n\\\\world!\\n\""})
	void testStringLiteral(String input) throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(string());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@Test
	void testStringLiteralFailureWrongEscape() throws Exception {
		String input = "\"apple/\"pie/\"\"";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(string());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo("\"apple/\"");
	}

	@Test
	@SuppressWarnings("java:S5976") // Similar tests should be grouped in a single Parameterized test
	void testStringLiteralFailureWrongStartingDelimiter() throws Exception {
		String input = "'apple\"";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(string());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"'apple\"\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a string literal (delimiter: '\"', escape character: '\\')."
			);
	}

	@Test
	void testStringLiteralFailureWrongClosingDelimiter() throws Exception {
		String input = "\"apple'";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(string());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\"apple'\n" +
				"       ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a string literal (delimiter: '\"', escape character: '\\')."
			);
	}

	@Test
	void testStringLiteralFailureEmptyString() throws Exception {
		String input = "";
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(string());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a string literal (delimiter: '\"', escape character: '\\')."
			);
	}
}

