package com.h119.syntx.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class ZeroOrMoreTest {
	@ParameterizedTest
	@ValueSource(strings = {"", "a", "aa", "abc", "aabbcc"})
	void testZeroOrMore(String input) throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(zeroOrMore(character("abc")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@ParameterizedTest
	@ValueSource(strings = {"", "a", "aa", "abc", "aabbcc"})
	void testKleene(String input) throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(kleene(character("abc")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}
}
