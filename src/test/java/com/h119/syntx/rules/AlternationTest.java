package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class AlternationTest {
	@ParameterizedTest
	@ValueSource(strings = {"a", "b", "c", "d", "e"})
	void testAlternation(String input) throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString(input));

		main.set(character("abc").or(character("de")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(input);
	}

	@Test
	void testAlternationFailure() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("x"));

		main.set(character("abc").or(character("de")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"x\n" +
				"^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character from a set: {'a', 'b', 'c'}."
			);
	}
}

