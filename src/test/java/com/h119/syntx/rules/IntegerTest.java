package com.h119.syntx.rules;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class IntegerTest {
	@ParameterizedTest
	@ValueSource(strings = {"-231", "54", "0", "-0", "+1953"})
	void testInteger(String number) throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString(number));

		main.set(integer());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(number);
	}

	@Test
	void testIntegerFailure() throws Exception {
		var main = rule().called("int").build();
		var context = new Context(fromString("-+"));

		main.set(integer());

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();

		assertThat(success).isFalse();

		String message =
			result
				.getError()
				.get()
				.getErrorMessage(context.getText());

		assertThat(message)
			.isEqualTo(
				"-+\n" +
				" ^\n" +
				"ERROR: while parsing rule \"int\" the parser expected to find an integer."
			);
	}
}
