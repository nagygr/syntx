package com.h119.syntx.util.text;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

class ParsableFileTest {
	@Test
	void testGetLineContaining() throws Exception {
		var p = new ParsableFile("test_data/small_text");
		LineIndex le = p.getLineContaining(4);

		assertThat(le.getIndex()).isEqualTo(1);
		assertThat(le.getLine()).isEqualTo("cde");
	}

	@Test
	void testSubstring() throws Exception {
		var p = new ParsableFile("test_data/small_text");
		String s = p.substring(1,8);

		assertThat(s).isEqualTo("b\ncde\nf");
	}

	@Test
	void testFileSize() throws Exception {
		var p = new ParsableFile("test_data/small_text");

		int preSize = p.length();
		p.charAt(0);
		int postSize = p.length();

		assertThat(preSize)
			.isEqualTo(postSize)
			.isEqualTo(12);
	}
}
