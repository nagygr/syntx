package com.h119.syntx.util.text;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

class ParsableStringTest {
	@Test
	void testMultilineInputWithGetLineContaining() throws Exception {
		var p = new ParsableString("ab\ncde\nfghj");
		LineIndex le = p.getLineContaining(4);

		assertThat(le.getIndex()).isEqualTo(1);
		assertThat(le.getLine()).isEqualTo("cde");
	}
}
