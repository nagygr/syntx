package com.h119.syntx.util.deserial;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.Data;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.util.error.ParseError;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.parsing.ParseResult;

import static com.h119.syntx.Syntx.*;

class AstToObjectParserTest {
	@Data
	private static class A {
		private String field;
		private List<Integer> list;
		private Result result;
		private Number number;
		private int podInt;

		{
			list = new ArrayList<>();
		}
	}

	@Test
	void testObjectGenerationAndFieldAssignment() throws Exception {
		var main = rule().called("Main").map(s -> new A()).build();
		var fields = rule().build();
		var field = rule().called("CharField").saveAs(field(A.class, String.class, A::setField)).build();
		var yar = rule().called("IntField").map(Integer::valueOf).saveAs(listElement(A.class, Integer.class, A::getList)).build();
		var myresult = rule().called("Result").map(s -> new Result()).saveAs(field(A.class, Result.class, A::setResult)).build();
		var to = rule().called("to").map(Integer::valueOf).saveAs(field(Result.class, Integer.class, Result::setTo)).build();
		var from = rule().called("from").map(Integer::valueOf).saveAs(field(Result.class, Integer.class, Result::setFrom)).build();
		var number = rule().called("Number").map(Integer::valueOf).saveAs(field(A.class, Number.class, A::setNumber)).build();
		var pod = rule().called("pod").map(Integer::valueOf).saveAs(field(A.class, Integer.class, A::setPodInt)).build();
		var digits = rule().build();

		var context = new Context(fromString("ab123456"));
		var result = new Result();
		var node = new AstNode("<root>");
		var errorHandler = new ParseErrorHandler();

		main    .set(character("axy").cat(fields));
		fields  .set(field.cat(yar).cat(yar).cat(myresult).cat(number).cat(pod));
		field   .set(character("bwq"));
		yar     .set(digits);
		myresult.set(from.cat(to));
		from    .set(digits);
		to      .set(digits);
		number  .set(digits);
		pod     .set(digits);
		digits  .set(character("0123456789"));

		boolean success = main.match(context, result, node, errorHandler);

		assertThat(success).isTrue();

		AstToObjectParser atop = new AstToObjectParser(context);
		Object o = atop.parseAst(node);

		assertThat(o)
			.isNotNull()
			.isInstanceOf(A.class);

		A a = (A)o;

		assertThat(a.getField()).isEqualTo("b");

		List<Integer> aList = a.getList();

		assertThat(aList).isNotNull();
		assertThat(aList.size()).isEqualTo(2);
		assertThat(aList.get(0)).isEqualTo(1);
		assertThat(aList.get(1)).isEqualTo(2);

		Result aResult = a.getResult();

		assertThat(aResult).isNotNull();
		assertThat(aResult.getFrom()).isEqualTo(3);
		assertThat(aResult.getTo()).isEqualTo(4);

		Number aNumber = a.getNumber();

		assertThat(aNumber)
			.isNotNull()
			.isEqualTo(Integer.valueOf(5));

		int aPod = a.getPodInt();

		assertThat(aPod).isEqualTo(6);
	}

	@Test
	void testErrorMessages() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("ab"));
		var result = new Result();
		var node = new AstNode("<root>");
		var errorHandler = new ParseErrorHandler();

		main.set(character("abc").cat(character("def")));

		boolean success = main.match(context, result, node, errorHandler);

		assertThat(success).isFalse();
		assertThat(errorHandler.hasError()).isTrue();

		var error = errorHandler.getError();

		assertThat(error.getPosition()).isEqualTo(1);
		assertThat(error.getRuleName())
			.isPresent()
			.contains("main");

		assertThat(error.getErrorMessage(fromString("ab")))
			.isEqualTo(
				"ab\n" +
				" ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character from a set: {'d', 'e', 'f'}."
			);
	}

	@Test
	void testSyntxParseWithSuccess() throws Exception {
		var main = rule().called("Main").map(s -> new A()).build();
		var fields = rule().build();
		var field = rule().called("CharField").saveAs(field(A.class, String.class, A::setField)).build();
		var yar = rule().called("IntField").map(Integer::valueOf).saveAs(listElement(A.class, Integer.class, A::getList)).build();
		var myresult = rule().called("Result").map(s -> new Result()).saveAs(field(A.class, Result.class, A::setResult)).build();
		var to = rule().called("to").map(Integer::valueOf).saveAs(field(Result.class, Integer.class, Result::setTo)).build();
		var from = rule().called("from").map(Integer::valueOf).saveAs(field(Result.class, Integer.class, Result::setFrom)).build();
		var number = rule().called("Number").map(Integer::valueOf).saveAs(field(A.class, Number.class, A::setNumber)).build();
		var pod = rule().called("pod").map(Integer::valueOf).saveAs(field(A.class, Integer.class, A::setPodInt)).build();
		var digits = rule().build();

		var context = new Context(fromString("ab123456"));

		main    .set(character("axy").cat(fields));
		fields  .set(field.cat(yar).cat(yar).cat(myresult).cat(number).cat(pod));
		field   .set(character("bwq"));
		yar     .set(digits);
		myresult.set(from.cat(to));
		from    .set(digits);
		to      .set(digits);
		number  .set(digits);
		pod     .set(digits);
		digits  .set(character("0123456789"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();
		AstNode node = result.getRoot();

		assertThat(success).isTrue();

		AstToObjectParser atop = new AstToObjectParser(context);
		Object o = atop.parseAst(node);

		assertThat(o)
			.isNotNull()
			.isInstanceOf(A.class);

		A a = (A)o;

		assertThat(a.getField()).isEqualTo("b");

		List<Integer> aList = a.getList();

		assertThat(aList).isNotNull();
		assertThat(aList.size()).isEqualTo(2);
		assertThat(aList.get(0)).isEqualTo(1);
		assertThat(aList.get(1)).isEqualTo(2);

		Result aResult = a.getResult();

		assertThat(aResult).isNotNull();
		assertThat(aResult.getFrom()).isEqualTo(3);
		assertThat(aResult.getTo()).isEqualTo(4);

		Number aNumber = a.getNumber();

		assertThat(aNumber)
			.isNotNull()
			.isEqualTo(Integer.valueOf(5));

		int aPod = a.getPodInt();

		assertThat(aPod).isEqualTo(6);
	}

	@Test
	void testSyntxParseWithFailure() throws Exception {
		var main = rule().called("main").build();
		var context = new Context(fromString("ab"));

		main.set(character("abc").cat(character("def")));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();
		Optional<ParseError> error = result.getError();

		assertThat(success).isFalse();
		assertThat(error).isPresent();

		assertThat(error.get().getPosition()).isEqualTo(1);
		assertThat(error.get().getRuleName())
			.isPresent()
			.contains("main");

		assertThat(error.get().getErrorMessage(fromString("ab")))
			.isEqualTo(
				"ab\n" +
				" ^\n" +
				"ERROR: while parsing rule \"main\" the parser expected to find a character from a set: {'d', 'e', 'f'}."
			);
	}

	@Test
	void testSyntxParseFileWithSuccess() throws Exception {
		var main = rule().called("Main").map(s -> new A()).build();
		var fields = rule().build();
		var field = rule().called("CharField").saveAs(field(A.class, String.class, A::setField)).build();
		var yar = rule().called("IntField").map(Integer::valueOf).saveAs(listElement(A.class, Integer.class, A::getList)).build();
		var myresult = rule().called("Result").map(s -> new Result()).saveAs(field(A.class, Result.class, A::setResult)).build();
		var to = rule().called("to").map(Integer::valueOf).saveAs(field(Result.class, Integer.class, Result::setTo)).build();
		var from = rule().called("from").map(Integer::valueOf).saveAs(field(Result.class, Integer.class, Result::setFrom)).build();
		var number = rule().called("Number").map(Integer::valueOf).saveAs(field(A.class, Number.class, A::setNumber)).build();
		var pod = rule().called("pod").map(Integer::valueOf).saveAs(field(A.class, Integer.class, A::setPodInt)).build();
		var digits = rule().build();

		var context = new Context(fromFile("test_data/simple_parsing"));

		main    .set(character("axy").cat(fields));
		fields  .set(field.cat(yar).cat(yar).cat(myresult).cat(number).cat(pod));
		field   .set(character("bwq"));
		yar     .set(digits);
		myresult.set(from.cat(to));
		from    .set(digits);
		to      .set(digits);
		number  .set(digits);
		pod     .set(digits);
		digits  .set(character("0123456789"));

		ParseResult result = parse(context, main);
		boolean success = result.hasSucceeded();
		AstNode node = result.getRoot();

		assertThat(success).isTrue();

		AstToObjectParser atop = new AstToObjectParser(context);
		Object o = atop.parseAst(node);

		assertThat(o)
			.isNotNull()
			.isInstanceOf(A.class);

		A a = (A)o;

		assertThat(a.getField()).isEqualTo("b");

		List<Integer> aList = a.getList();

		assertThat(aList).isNotNull();
		assertThat(aList.size()).isEqualTo(2);
		assertThat(aList.get(0)).isEqualTo(1);
		assertThat(aList.get(1)).isEqualTo(2);

		Result aResult = a.getResult();

		assertThat(aResult).isNotNull();
		assertThat(aResult.getFrom()).isEqualTo(3);
		assertThat(aResult.getTo()).isEqualTo(4);

		Number aNumber = a.getNumber();

		assertThat(aNumber)
			.isNotNull()
			.isEqualTo(Integer.valueOf(5));

		int aPod = a.getPodInt();

		assertThat(aPod).isEqualTo(6);
	}
}
