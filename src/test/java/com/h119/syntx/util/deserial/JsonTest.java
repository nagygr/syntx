package com.h119.syntx.util.deserial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import com.h119.syntx.rules.Rule;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;

import lombok.ToString;

import static com.h119.syntx.Syntx.*;

class JsonTest {
	private interface JsonValue {
		default Optional<Double> asNumber() {
			throw new UnsupportedOperationException("JsonValue is not a number.");
		}

		default Optional<String> asString() {
			throw new UnsupportedOperationException("JsonValue is not a string.");
		}

		default Optional<Boolean> asBoolean() {
			throw new UnsupportedOperationException("JsonValue is not a boolean.");
		}

		default Optional<JsonObject> asObject() {
			throw new UnsupportedOperationException("JsonValue is not a JSON object.");
		}

		default Optional<JsonArray> asArray() {
			throw new UnsupportedOperationException("JsonValue is not a JSON array.");
		}
	}

	@ToString
	private static class JsonNull implements JsonValue {
		@Override
		public Optional<Double> asNumber() {
			return Optional.empty();
		}

		@Override
		public Optional<String> asString() {
			return Optional.empty();
		}

		@Override
		public Optional<Boolean> asBoolean() {
			return Optional.empty();
		}

		@Override
		public Optional<JsonObject> asObject() {
			return Optional.empty();
		}

		@Override
		public Optional<JsonArray> asArray() {
			return Optional.empty();
		}
	}

	@ToString
	private static class JsonNumber implements JsonValue {
		private double value;

		public JsonNumber(String value) {
			this.value = Double.valueOf(value);
		}

		@Override
		public Optional<Double> asNumber() {
			return Optional.of(value);
		}
	}

	@ToString
	private static class JsonString implements JsonValue {
		private String value;

		public JsonString(String value) {
			this.value = value.substring(1, value.length() - 1);
		}

		@Override
		public Optional<String> asString() {
			return Optional.of(value);
		}
	}

	@ToString
	private static class JsonBoolean implements JsonValue {
		private boolean value;

		public JsonBoolean(String value) {
			this.value = Boolean.valueOf(value);
		}

		@Override
		public Optional<Boolean> asBoolean() {
			return Optional.of(value);
		}
	}

	private interface SettableField {
		void setField(JsonValue value);
	}

	@ToString
	private static class JsonObject implements JsonValue, SettableField {
		private Map<String, JsonValue> fields;

		@ToString.Exclude
		private String currentField;

		public JsonObject() {
			fields = new HashMap<>();
			currentField = "";
		}

		public boolean hasField(String name) {
			return fields.containsKey(name);
		}

		public JsonValue getField(String name) {
			JsonValue field = fields.get(name);

			if (field == null)
				throw new IllegalArgumentException(
					String.format(
						"The field \"%s\" is not present in the JsonObject",
						name
					)
				);

			return field;
		}

		public void selectField(String name) {
			currentField = name;
		}

		@Override
		public void setField(JsonValue value) {
			fields.put(currentField, value);
		}

		@Override
		public Optional<JsonObject> asObject() {
			return Optional.of(this);
		}
	}

	@ToString
	private static class JsonArray implements JsonValue, SettableField {
		private List<JsonValue> elements;

		public JsonArray() {
			elements = new ArrayList<>();
		}

		@Override
		public void setField(JsonValue value) {
			elements.add(value);
		}

		public JsonValue getElement(int i) {
			return elements.get(i);
		}

		public int size() {
			return elements.size();
		}

		@Override
		public Optional<JsonArray> asArray() {
			return Optional.of(this);
		}
	}

	private static class JsonGrammar {
		private Rule json;
		private Rule value;
		private Rule nullValue;
		private Rule number;
		private Rule str;
		private Rule bool;
		private Rule object;
		private Rule objectElement;
		private Rule fieldName;
		private Rule array;

		public JsonGrammar() {
			json = rule().build();
			value = rule().build();
			number = rule().called("number").map(JsonNumber::new).saveAs(field(SettableField.class, JsonValue.class, SettableField::setField)).build();
			nullValue = rule().called("null").map(s -> new JsonNull()).saveAs(field(SettableField.class, JsonNull.class, SettableField::setField)).build();
			str = rule().called("str").map(JsonString::new).saveAs(field(SettableField.class, JsonValue.class, SettableField::setField)).build();
			bool = rule().called("bool").map(JsonBoolean::new).saveAs(field(SettableField.class, JsonValue.class, SettableField::setField)).build();
			object = rule().called("object").map(s -> new JsonObject()).saveAs(field(SettableField.class, JsonValue.class, SettableField::setField)).build();
			objectElement = rule().build();
			fieldName = rule().called("name").map(s -> s.substring(1,s.length()-1)).saveAs(field(JsonObject.class, String.class, JsonObject::selectField)).build();
			array = rule().called("array").map(s -> new JsonArray()).saveAs(field(SettableField.class, JsonValue.class, SettableField::setField)).build();

			json.set(value);

			value.set(
				ws(number)
				.or(ws(str))
				.or(ws(bool))
				.or(ws(object))
				.or(ws(array))
				.or(ws(nullValue))
			);

			nullValue.set(ws(keyword("null")));
			number.set(ws(real()));
			str.set(ws(string()));
			bool.set(ws(keyword("true")).or(ws(keyword("false"))));
			object.set(
				ws(character("{"))
				.cat(objectElement)
				.cat(
					zeroOrMore(
						ws(character(","))
						.cat(objectElement)
					)
				)
				.cat(ws(character("}")))
			);

			objectElement.set(
				fieldName
				.cat(ws(character(":")))
				.cat(ws(value))
			);

			fieldName.set(ws(string()));

			array.set(
				ws(character("["))
				.cat(ws(value))
				.cat(
					zeroOrMore(
						ws(character(","))
						.cat(ws(value))
					)
				)
				.cat(ws(character("]")))
			);
		}

		public Rule mainRule() {
			return json;
		}
	}

	@Test
	void testSimpleJsonMatch() throws Exception {
		var grammar = new JsonGrammar();
		var context = new Context(fromFile("test_data/simple.json"));

		ParseResult result = parse(context, grammar.mainRule());
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		String value =
			result
				.getRoot()
				.getChildren()
				.get(0)
				.getResult()
				.getValue(context.getText());

		assertThat(value).isEqualTo(
			"{\n" +
			"	\"number\": 23.4,\n" +
			"	\"boolean\": true,\n" +
			"	\"inner_class\": { \"country\": \"Denmark\" }\n" +
			"}"
		);
	}

	@Test
	void testSimpleJsonMatchWithObjects() throws Exception {
		var grammar = new JsonGrammar();
		var context = new Context(fromFile("test_data/simple.json"));

		ParseResult result = parse(context, grammar.mainRule());
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		var atop = new AstToObjectParser(context);
		Object o = atop.parseAst(result.getRoot());

		assertThat(o).isInstanceOf(JsonObject.class);

		JsonObject root = (JsonObject)o;

		assertThat(root).hasToString(
			"JsonTest.JsonObject(fields={number=JsonTest.JsonNumber(value=23.4), " +
			"boolean=JsonTest.JsonBoolean(value=true), inner_class=JsonTest.JsonObject(" +
			"fields={country=JsonTest.JsonString(value=Denmark)})})"
		);

		double number = root.getField("number").asNumber().orElseThrow();
		assertThat(number).isCloseTo(23.4, offset(1e-3));

		boolean bool = root.getField("boolean").asBoolean().orElseThrow();
		assertThat(bool).isTrue();

		JsonObject inner = root.getField("inner_class").asObject().orElseThrow();
		String country = inner.getField("country").asString().orElseThrow();
		assertThat(country).isEqualTo("Denmark");
	}

	@Test
	void testSingleNumberJsonMatchWithObjects() throws Exception {
		var grammar = new JsonGrammar();
		var context = new Context(fromFile("test_data/number.json"));

		ParseResult result = parse(context, grammar.mainRule());
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		var atop = new AstToObjectParser(context);
		Object o = atop.parseAst(result.getRoot());

		assertThat(o).isInstanceOf(JsonNumber.class);
		JsonNumber root = (JsonNumber)o;

		assertThat(root.asNumber().orElseThrow()).isCloseTo(42.0, offset(1e-3));
		assertThat(root).hasToString("JsonTest.JsonNumber(value=42.0)");
	}

	@Test
	void testObjectWithArrayJsonMatchWithObjects() throws Exception {
		var grammar = new JsonGrammar();
		var context = new Context(fromFile("test_data/object_array.json"));

		ParseResult result = parse(context, grammar.mainRule());
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		var atop = new AstToObjectParser(context);
		Object o = atop.parseAst(result.getRoot());

		assertThat(o).isInstanceOf(JsonObject.class);

		JsonObject root = (JsonObject)o;

		assertThat(root).hasToString(
			"JsonTest.JsonObject(fields={number=JsonTest.JsonNumber(value=23.4), " +
			"boolean=JsonTest.JsonBoolean(value=true), inner_array=JsonTest.JsonArray(" +
			"elements=[JsonTest.JsonNumber(value=12.0), JsonTest.JsonNumber(value=23.0), " +
			"JsonTest.JsonNumber(value=34.0), JsonTest.JsonNumber(value=45.0)])})"
		);

		double number = root.getField("number").asNumber().orElseThrow();
		assertThat(number).isCloseTo(23.4, offset(1e-3));

		boolean bool = root.getField("boolean").asBoolean().orElseThrow();
		assertThat(bool).isTrue();

		JsonArray inner = root.getField("inner_array").asArray().orElseThrow();
		double value = inner.getElement(0).asNumber().orElseThrow();
		assertThat(value).isCloseTo(12.0, offset(1e-3));
	}

	@Test
	void testArrayOfArrayJsonMatchWithObjects() throws Exception {
		var grammar = new JsonGrammar();
		var context = new Context(fromFile("test_data/array_array_object.json"));

		ParseResult result = parse(context, grammar.mainRule());
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		var atop = new AstToObjectParser(context);
		Object o = atop.parseAst(result.getRoot());

		assertThat(o).isInstanceOf(JsonArray.class);

		JsonArray root = (JsonArray)o;
		assertThat(root.size()).isEqualTo(2);

		JsonArray inner = root.getElement(0).asArray().orElseThrow();
		assertThat(inner.size()).isEqualTo(2);

		JsonObject object = inner.getElement(0).asObject().orElseThrow();
		String world = object.getField("hello").asString().orElseThrow();
		assertThat(world).isEqualTo("world");

		assertThat(
			inner
			.getElement(1)
			.asObject()
			.orElseThrow()
			.getField("hej")
			.asString()
			.orElseThrow()
		)
			.isEqualTo("verden");

		assertThat(
			root
			.getElement(1)
			.asArray()
			.orElseThrow()
			.getElement(0)
			.asObject()
			.orElseThrow()
			.getField("hallo")
			.asString()
			.orElseThrow()
		)
			.isEqualTo("Welt");

		assertThat(root).hasToString(
			"JsonTest.JsonArray(elements=[JsonTest.JsonArray(" +
			"elements=[JsonTest.JsonObject(fields={hello=JsonTest.JsonString(value=world)}), " +
			"JsonTest.JsonObject(fields={hej=JsonTest.JsonString(value=verden)})]), " +
			"JsonTest.JsonArray(elements=[JsonTest.JsonObject(fields={hallo=JsonTest.JsonString(value=Welt)})])])"
		);
	}

	@Test
	void testObjectWithArrayAndNullJsonMatchWithObjects() throws Exception {
		var grammar = new JsonGrammar();
		var context = new Context(fromFile("test_data/object_array_w_null.json"));

		ParseResult result = parse(context, grammar.mainRule());
		boolean success = result.hasSucceeded();

		assertThat(success).isTrue();

		var atop = new AstToObjectParser(context);
		Object o = atop.parseAst(result.getRoot());

		assertThat(o).isInstanceOf(JsonObject.class);

		JsonObject root = (JsonObject)o;

		assertThat(root).hasToString(
			"JsonTest.JsonObject(fields={number=JsonTest.JsonNumber(value=23.4), " +
			"inner_array=JsonTest.JsonArray(elements=[JsonTest.JsonNumber(value=12.0), " +
			"JsonTest.JsonNumber(value=23.0), JsonTest.JsonNumber(value=34.0), " +
			"JsonTest.JsonNumber(value=45.0)]), object=JsonTest.JsonNull()})"
		);

		double number = root.getField("number").asNumber().orElseThrow();
		assertThat(number).isCloseTo(23.4, offset(1e-3));

		Optional<JsonObject> nullField = root.getField("object").asObject();
		assertThat(nullField).isEmpty();

		JsonArray inner = root.getField("inner_array").asArray().orElseThrow();
		double value = inner.getElement(0).asNumber().orElseThrow();
		assertThat(value).isCloseTo(12.0, offset(1e-3));
	}
}
