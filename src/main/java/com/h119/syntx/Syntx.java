package com.h119.syntx;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

import com.h119.syntx.rules.BaseRule;
import com.h119.syntx.rules.Character;
import com.h119.syntx.rules.Epsilon;
import com.h119.syntx.rules.Identifier;
import com.h119.syntx.rules.Integer;
import com.h119.syntx.rules.Keyword;
import com.h119.syntx.rules.NonDecimal;
import com.h119.syntx.rules.NotCharacter;
import com.h119.syntx.rules.NotRange;
import com.h119.syntx.rules.OneOrMore;
import com.h119.syntx.rules.Option;
import com.h119.syntx.rules.Range;
import com.h119.syntx.rules.Real;
import com.h119.syntx.rules.Rule;
import com.h119.syntx.rules.RuleBuilder;
import com.h119.syntx.rules.StringLiteral;
import com.h119.syntx.rules.Substring;
import com.h119.syntx.rules.Whitespace;
import com.h119.syntx.rules.ZeroOrMore;
import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.deserial.FieldSetter;
import com.h119.syntx.util.deserial.ListAdder;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.ParseResult;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;
import com.h119.syntx.util.text.ParsableFile;
import com.h119.syntx.util.text.ParsableString;

public final class Syntx {
	private Syntx() {
		throw new IllegalStateException("Utility class");
	}

	public static NonDecimal binary() {
		return new NonDecimal("0b", NonDecimal.BINARY_DIGITS);
	}

	public static NonDecimal binary(String prefix) {
		return new NonDecimal(prefix, NonDecimal.BINARY_DIGITS);
	}

	public static Character character(String characterSet) {
		return new Character(characterSet);
	}

	public static Epsilon eps() {
		return new Epsilon();
	}

	public static Epsilon epsilon() {
		return new Epsilon();
	}

	public static NonDecimal hexadecimal() {
		return new NonDecimal("0x", NonDecimal.HEXADECIMAL_DIGITS);
	}

	public static NonDecimal hexadecimal(String prefix) {
		return new NonDecimal(prefix, NonDecimal.HEXADECIMAL_DIGITS);
	}

	public static Identifier identifier() {
		return new Identifier();
	}

	public static Identifier identifier(String extraCharacters) {
		return new Identifier(extraCharacters);
	}

	public static Integer integer() {
		return new Integer();
	}

	public static Keyword keyword(String theKeyword) {
		return new Keyword(theKeyword);
	}

	public static ZeroOrMore kleene(BaseRule element) {
		return new ZeroOrMore(element);
	}

	public static NotCharacter notCharacter(String characterSet) {
		return new NotCharacter(characterSet);
	}

	public static NotRange notRange(char lowerBound, char upperBound) {
		return new NotRange(lowerBound, upperBound);
	}

	public static OneOrMore oneOrMore(BaseRule element) {
		return new OneOrMore(element);
	}

	public static Option option(BaseRule element) {
		return new Option(element);
	}

	public static Range range(char lowerBound, char upperBound) {
		return new Range(lowerBound, upperBound);
	}

	public static Real real() {
		return new Real('.');
	}

	public static Real real(char decimalMark) {
		return new Real(decimalMark);
	}

	public static StringLiteral string(char delimiter, char escape) {
		return new StringLiteral(delimiter, escape);
	}

	public static StringLiteral string() {
		return new StringLiteral('\"', '\\');
	}

	public static StringLiteral stringLiteral(char delimiter, char escape) {
		return new StringLiteral(delimiter, escape);
	}

	public static StringLiteral stringLiteral() {
		return new StringLiteral('\"', '\\');
	}

	public static Substring substring(String word) {
		return new Substring(word);
	}

	public static Whitespace ws(BaseRule element) {
		return new Whitespace(element);
	}

	public static ZeroOrMore zeroOrMore(BaseRule element) {
		return new ZeroOrMore(element);
	}

	public static RuleBuilder rule() {
		return new RuleBuilder();
	}

	public static <T,V> FieldSetter<T,V> field(Class<T> objectType, Class<V> valueType, BiConsumer<T,V> setter) {
		return new FieldSetter<>(objectType, valueType, setter);
	}

	public static <T,V> ListAdder<T,V> listElement(Class<T> objectType, Class<V> valueType, Function<T, List<V>> listGetter) {
		return new ListAdder<>(objectType, valueType, listGetter);
	}

	public static ParseResult parse(Context context, Rule mainRule) {
		var result = new Result();
		var node = new AstNode("<root>");
		var errorHandler = new ParseErrorHandler();

		boolean success = mainRule.match(context, result, node, errorHandler);

		return new ParseResult(node, success ? Optional.empty() : Optional.of(errorHandler.getError()));
	}

	public static Parsable fromString(String text) {
		return new ParsableString(text);
	}

	public static Parsable fromFile(String path) throws IOException {
		return new ParsableFile(path);
	}
}
