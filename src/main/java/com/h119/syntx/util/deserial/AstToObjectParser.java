package com.h119.syntx.util.deserial;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;
import java.util.function.Function;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.parsing.Context;

public class AstToObjectParser {
	private Context context;
	private Deque<Object> objectStack;

	public AstToObjectParser(Context context) {
		this.context = context;
		objectStack = new ArrayDeque<>();
	}

	public Object parseAst(AstNode node) throws Exception {
		String nodeName = node.getName();
		Optional<Function<String, Object>> mapper = node.getTypeMapper();
		Optional<ResultHandler> resultHandler = node.getResultHandler();

		if (nodeName.equals("<root>")) {
			var children = node.getChildren();

			if (children.isEmpty())
				throw new IllegalStateException("Root element has no children.");

			return parseAst(children.get(0));
		}

		String value = node.getResult().getValue(context.getText());
		Object object = mapper.isPresent() ? mapper.get().apply(value) : value;

		if (resultHandler.isPresent()) {
			Object owner = objectStack.peek();
			if (owner != null)
				resultHandler.get().apply(owner, object);
		}

		objectStack.push(object);

		for (var child: node.getChildren()) {
			parseAst(child);
		}

		return objectStack.pop();
	}
}
