package com.h119.syntx.util.deserial;

import java.util.List;
import java.util.function.Function;

public class ListAdder<T,V> implements ResultHandler {
	Class<T> objectType;
	Class<V> valueType;
	Function<T, List<V>> listGetter;

	public ListAdder(Class<T> objectType, Class<V> valueType, Function<T, List<V>> listGetter) {
		this.objectType = objectType;
		this.valueType = valueType;
		this.listGetter = listGetter;
	}

	@Override
	public void apply(Object owner, Object currentValue) {
		if (!(objectType.isInstance(owner))) {
			throw new IllegalArgumentException(
				String.format(
					"Object type in ListAdder is invalid (%s)%n",
					owner.getClass().getCanonicalName()
				)
			);
		}

		if (!(valueType.isInstance(currentValue))) {
			throw new IllegalArgumentException(
				String.format(
					"Value type in ListAdder is invalid (%s)%n",
					currentValue.getClass().getCanonicalName()
				)
			);
		}

		T object = objectType.cast(owner);
		V value = valueType.cast(currentValue);

		List<V> list = listGetter.apply(object);
		list.add(value);
	}
}

