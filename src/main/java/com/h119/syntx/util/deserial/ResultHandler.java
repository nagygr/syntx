package com.h119.syntx.util.deserial;

public interface ResultHandler {
	void apply(Object owner, Object currentValue);
}
