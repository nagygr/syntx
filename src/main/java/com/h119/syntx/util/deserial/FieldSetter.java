package com.h119.syntx.util.deserial;

import java.util.function.BiConsumer;

public class FieldSetter<T,V> implements ResultHandler {
	Class<T> objectType;
	Class<V> valueType;
	BiConsumer<T,V> setter;

	public FieldSetter(Class<T> objectType, Class<V> valueType, BiConsumer<T,V> setter) {
		this.objectType = objectType;
		this.valueType = valueType;
		this.setter = setter;
	}

	@Override
	public void apply(Object owner, Object currentValue) {
		if (!(objectType.isInstance(owner))) {
			throw new IllegalArgumentException(
				String.format(
					"Object type in FieldSetter is invalid (%s)%n",
					owner.getClass().getCanonicalName()
				)
			);
		}

		if (!(valueType.isInstance(currentValue))) {
			throw new IllegalArgumentException(
				String.format(
					"Value type in FieldSetter is invalid (%s)%n",
					currentValue.getClass().getCanonicalName()
				)
			);
		}

		T object = objectType.cast(owner);
		V value = valueType.cast(currentValue);

		setter.accept(object, value);
	}
}
