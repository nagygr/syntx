package com.h119.syntx.util.error;

import java.util.Optional;

import lombok.Getter;
import lombok.Setter;

import com.h119.syntx.util.text.LineIndex;
import com.h119.syntx.util.text.Parsable;

public class ParseError {
	@Getter
	int position;

	@Getter
	String charLevelMessage;

	@Getter @Setter
	Optional<String> ruleName;

	public ParseError(int position, String charLevelMessage) {
		this.position = position;
		this.charLevelMessage = charLevelMessage;
		ruleName = Optional.empty();
	}

	public String getErrorMessage(Parsable text) {
		LineIndex lineIndex = text.getLineContaining(position);
		var spaces = new String(new char[lineIndex.getIndex()]).replace("\0", " ");

		return
			String.format(
				"%s\n" +
				"%s^\n" +
				"ERROR: while parsing rule \"%s\" the parser expected to find %s.",
				lineIndex.getLine(),
				spaces,
				ruleName.get(),
				charLevelMessage
			);

	}
}
