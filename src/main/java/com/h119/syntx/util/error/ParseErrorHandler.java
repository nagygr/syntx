package com.h119.syntx.util.error;

import java.util.Optional;

public class ParseErrorHandler {
	private Optional<ParseError> parseError;

	public ParseErrorHandler() {
		parseError = Optional.empty();
	}

	public boolean hasError() {
		return parseError.isPresent();
	}

	public ParseError getError() {
		return
			parseError
				.orElseThrow(
					() -> new IllegalStateException("An empty ParseError was requested.")
				);

	}

	public void setError(ParseError anError) {
		if (parseError.isEmpty() || parseError.get().getPosition() < anError.getPosition()) {
			parseError = Optional.of(anError);
		}
	}
}
