package com.h119.syntx.util.parsing;

import lombok.Data;

import com.h119.syntx.util.text.Parsable;

@Data
public class Context {
	private Parsable text;
	private int position;

	public Context(Parsable text) {
		this.text = text;
		position = 0;
	}

	public Context(Context other) {
		text = other.text;
		position = other.position;
	}

	public Context set(Context other) {
		text = other.text;
		position = other.position;

		return this;
	}
}
