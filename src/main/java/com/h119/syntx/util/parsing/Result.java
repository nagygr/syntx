package com.h119.syntx.util.parsing;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.h119.syntx.util.text.Parsable;

@Data @AllArgsConstructor @NoArgsConstructor
public class Result {
	private int from;
	private int to;

	public Result(Result other) {
		from = other.from;
		to = other.to;
	}

	public String getValue(Parsable text) {
		return text.substring(from, to);
	}

	public Result set(Result other) {
		from = other.from;
		to = other.to;

		return this;
	}
}
