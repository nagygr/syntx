package com.h119.syntx.util.parsing;

import java.util.Optional;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseError;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value @AllArgsConstructor
public class ParseResult {
	private AstNode root;
	private Optional<ParseError> error;

	public boolean hasSucceeded() {
		return error.isEmpty();
	}
}
