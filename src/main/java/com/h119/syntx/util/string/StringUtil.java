package com.h119.syntx.util.string;

import com.h119.syntx.util.text.Parsable;

public final class StringUtil {
	private StringUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static boolean isCharacterInRange(char character, char lowerBound, char upperBound) {
		return lowerBound <= character && character <= upperBound;
	}

	public static boolean isCharacterInString(char character, String allowedCharacters) {
		return allowedCharacters.indexOf(character) >= 0;
	}

	public static boolean startsWithFromIndex(Parsable text, int index, String word) {
		int length = text.length();

		int wordIndex = 0;
		int wordLength = word.length();

		if (index > length - wordLength) return false;

		while (index < length && wordIndex < wordLength && text.charAt(index) == word.charAt(wordIndex)) {
			index += 1;
			wordIndex += 1;
		}

		return wordIndex == wordLength;
	}
}
