package com.h119.syntx.util.text;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.BiPredicate;

public class ParsableFile implements Parsable {
	private final String filePath;
	private int position;
	private final int bufferSize;
	private char[] buffer;
	private int calculatedFileLength;
	private int fileSize;

	private static final int DEFAULT_BUFFER_SIZE = 1 << 16;

	public ParsableFile(String filePath, int bufferSize) throws IOException {
		this.filePath = filePath;
		position = 0;
		this.bufferSize = bufferSize;
		buffer = new char[bufferSize];
		fileSize = (int)Files.size(Paths.get(filePath));

		calculatedFileLength = -1;

		readBuffer(buffer, position);
	}

	public ParsableFile(String filePath) throws IOException {
		this(filePath, DEFAULT_BUFFER_SIZE);
	}

	private void readBuffer(char[] buf, int from) {
		try (
			BufferedReader reader = new BufferedReader(new FileReader(filePath), buf.length);
		)
		{
			reader.skip(from);
			int charsRead = reader.read(buf);

			if (charsRead == -1) {
				throw new IndexOutOfBoundsException(String.format(
					"Index out of bounds (charsRead == -1): %d", from
				));
			}
			else if (charsRead < bufferSize) {
				calculatedFileLength = from + charsRead;
			}
		}
		catch (IOException ioe) {
			throw new AssertionError(String.format("Error while reading the file: %s", ioe));
		}
	}

	private boolean isIndexWithinBuffer(int index) {
		return index >= position && index < (position + bufferSize);
	}

	@SuppressWarnings("java:S1121") // Assignments should not be made from within sub-expressions
	private LineIndex getSubString(
		int fromIndex,
		BiPredicate<Integer, Character> backwardStopPredicate,
		BiPredicate<Integer, Character> forwardStopPredicate
	) {
		var builder = new StringBuilder();
		int currentIndex;
		char currentChar;

		builder.append(charAt(fromIndex));

		for (
			currentIndex = fromIndex - 1;
			currentIndex >= 0 && backwardStopPredicate.test(currentIndex, currentChar = charAt(currentIndex));
			--currentIndex
		) {
			builder.insert(0, currentChar);
		}

		int newFromIndex = builder.length() - 1;

		for (
			currentIndex = fromIndex + 1;
			currentIndex < fileSize && forwardStopPredicate.test(currentIndex, currentChar = charAt(currentIndex));
			++currentIndex
		) {
			builder.append(currentChar);
		}

		return new LineIndex(builder.toString(), newFromIndex);
	}

	@Override
	public char charAt(int index) {
		if (!isIndexWithinBuffer(index)) {
			if (index >= position + bufferSize && index < position + (int)(1.9 * bufferSize)) {
				position = position + (int)(0.9 * bufferSize);
			}
			else if (index < position && index >= position - (int)(0.9 * bufferSize)) {
				position = Math.max(0, position - (int)(0.9 * bufferSize));
			}
			else {
				position = (index / bufferSize) * bufferSize;
			}

			readBuffer(buffer, position);
		}

		if (calculatedFileLength != -1 && index >= calculatedFileLength) {
			throw new IndexOutOfBoundsException(
				String.format(
					"Index out of bounds: %d (file size: %d)",
					index,
					calculatedFileLength
				)
			);
		}

		return buffer[index - position];
	}

	@Override
	public int length() {
		return calculatedFileLength == -1 ? fileSize : calculatedFileLength;
	}

	@Override
	public String substring(int from, int to) throws IndexOutOfBoundsException {
		return
			getSubString(from, (index, ch) -> false, (index, ch) -> index < to).getLine();
	}

	@Override
	public LineIndex getLineContaining(int index) {
		BiPredicate<Integer, Character> predicate = (i, ch) -> !Character.isWhitespace(ch);
		return getSubString(index, predicate, predicate);
	}
}

