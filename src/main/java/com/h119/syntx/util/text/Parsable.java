package com.h119.syntx.util.text;

public interface Parsable {
	int length();
	char charAt(int index) throws IndexOutOfBoundsException;
	String substring(int from, int to) throws IndexOutOfBoundsException;
	LineIndex getLineContaining(int index);
}
