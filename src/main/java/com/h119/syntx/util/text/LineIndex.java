package com.h119.syntx.util.text;

import lombok.Value;

@Value
public class LineIndex {
	String line;
	int index;
}
