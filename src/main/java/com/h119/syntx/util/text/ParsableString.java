package com.h119.syntx.util.text;

public class ParsableString implements Parsable {
	private String text;

	public ParsableString(String text) {
		this.text = text;
	}

	@Override
	public int length() {
		return text.length();
	}

	@Override
	public char charAt(int index) throws IndexOutOfBoundsException {
		return text.charAt(index);
	}

	@Override
	public String substring(int from, int to) throws IndexOutOfBoundsException {
		return text.substring(from, to);
	}

	@Override
	public LineIndex getLineContaining(int index) {
		int from = text.lastIndexOf("\n", index);
		int to = text.indexOf("\n", index);

		from = (from == -1) ? 0 : from + 1;
		if (to == -1) to = text.length();

		return
			new LineIndex(
				text.substring(from, to),
				index - from
			);
	}
}
