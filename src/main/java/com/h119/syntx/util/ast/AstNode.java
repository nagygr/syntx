package com.h119.syntx.util.ast;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import lombok.Data;

import com.h119.syntx.util.deserial.ResultHandler;
import com.h119.syntx.util.parsing.Result;

@Data
public class AstNode {
	private String name;
	private Result result;
	private List<AstNode> children;
	private Optional<Function<String, Object>> typeMapper;
	private Optional<ResultHandler> resultHandler;

	public AstNode(String name) {
		this.name = name;
		this.typeMapper = Optional.empty();
		this.resultHandler = Optional.empty();
		children = new ArrayList<>();
	}

	public void print() {
		printHelper(0);
	}

	@SuppressWarnings({"java:S106", "java:S1612"})
	// Standard outputs should not be used directly to log anything
	// Lambdas should be replaced with method references
	private void printHelper(int level) {
		String tabs = new String(new char[level]).replace("\0", "\t");
		System.out.format(
			"%s%s %s%n",
			tabs,
			name,
			Optional
				.ofNullable(result)
				.map(r -> r.toString())
				.orElse("")
		);
		for (var n: children) {
			n.printHelper(level + 1);
		}
	}
}
