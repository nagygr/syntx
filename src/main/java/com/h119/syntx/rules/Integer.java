package com.h119.syntx.rules;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static com.h119.syntx.util.string.StringUtil.*;

public class Integer extends BaseRule {
	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int length = text.length();
		int index = context.getPosition();

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		if (isCharacterInString(text.charAt(index), "+-")) {
			index += 1;
		}

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		if (text.charAt(index) == '0') {
			result.setFrom(context.getPosition());
			result.setTo(index + 1);
			context.setPosition(index + 1);

			return true;
		}
		else if (isCharacterInRange(text.charAt(index), '1', '9')) {
			index += 1;

			while (index < length && isCharacterInRange(text.charAt(index), '0', '9')) {
				index += 1;
			}

			result.setFrom(context.getPosition());
			result.setTo(index);
			context.setPosition(index);

			return true;
		}

		return reportError(errorHandler, index);
	}

	@Override
	public String toString() {
		return "an integer";
	}
}

