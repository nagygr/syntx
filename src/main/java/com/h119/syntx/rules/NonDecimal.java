package com.h119.syntx.rules;

import java.util.stream.Collectors;

import lombok.Getter;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static com.h119.syntx.util.string.StringUtil.*;

public class NonDecimal extends BaseRule {
	@Getter
	private String prefix;

	@Getter
	private String digits;

	public static final String BINARY_DIGITS = "01";
	public static final String HEXADECIMAL_DIGITS = "0123456789abcdefABCDEF";

	public NonDecimal(String prefix, String digits) {
		super();
		this.prefix = prefix;
		this.digits = digits;
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int length = text.length();
		int index = context.getPosition();

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		if (isCharacterInString(text.charAt(index), "+-")) {
			index += 1;
		}

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		if (startsWithFromIndex(text, index, prefix)) {
			index += prefix.length();
		}
		else {
			return reportError(errorHandler, index);
		}

		if (index >= length) {
			return reportError(errorHandler, index);
		}


		if (isCharacterInString(text.charAt(index), digits)) {
			index += 1;

			while (index < length && isCharacterInString(text.charAt(index), digits)) {
				index += 1;
			}

			result.setFrom(context.getPosition());
			result.setTo(index);
			context.setPosition(index);

			return true;
		}

		return reportError(errorHandler, index);
	}

	@Override
	public String toString() {
		return
			String.format(
				"a non-decimal (prefix: \"%s\", digits: {%s})",
				prefix,
				digits
					.chars()
					.mapToObj(
						c ->
							String.format(
								"'%c'",
								(char)c
							)
					)
					.collect(Collectors.joining(", "))
			);
	}
}

