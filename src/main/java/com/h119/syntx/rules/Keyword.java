package com.h119.syntx.rules;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static com.h119.syntx.util.string.StringUtil.*;

public class Keyword extends BaseRule {
	private String theKeyword;
	private String extraCharacters;

	public Keyword(String theKeyword) {
		super();
		this.theKeyword = theKeyword;
		this.extraCharacters = getExtraCharacters(theKeyword);
	}

	@Override
	public String toString() {
		return String.format("a keyword: %s", theKeyword);
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int length = text.length();
		int index = context.getPosition();

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		char current = text.charAt(index);

		if (
			isCharacterInRange(current, 'a', 'z') ||
			isCharacterInRange(current, 'A', 'Z') ||
			isCharacterInString(current, extraCharacters)
		) {
			index += 1;

			while (
				index < length && (
					isCharacterInRange(current, 'a', 'z') ||
					isCharacterInRange(current, 'A', 'Z') ||
					isCharacterInRange(current, '0', '9') ||
					isCharacterInString(current, extraCharacters)
				)
			) {
				index += 1;
				if (index < length) current = text.charAt(index);
			}

			if (text.substring(context.getPosition(), index).equals(theKeyword)) {
				result.setFrom(context.getPosition());
				result.setTo(index);
				context.setPosition(index);

				return true;
			}
		}

		return reportError(errorHandler, index);
	}

	private String getExtraCharacters(String keyword) {
		StringBuilder extras = new StringBuilder();

		for (int i = 0; i < keyword.length(); ++i) {
			char c = keyword.charAt(i);

			if (!
					(
						isCharacterInRange(c, 'a', 'z') ||
						isCharacterInRange(c, 'A', 'Z') ||
						isCharacterInRange(c, '0', '9')
					)
			)
				extras.append(c);
		}

		return extras.toString();
	}
}
