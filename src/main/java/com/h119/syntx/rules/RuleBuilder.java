package com.h119.syntx.rules;

import java.util.Optional;
import java.util.function.Function;

import com.h119.syntx.util.deserial.ResultHandler;

public class RuleBuilder {
	private Optional<String> name;
	private Optional<Function<String, Object>> typeMapper;
	private Optional<ResultHandler> resultHandler;

	public RuleBuilder() {
		name = Optional.empty();
		typeMapper = Optional.empty();
		resultHandler = Optional.empty();
	}

	public RuleBuilder called(String name) {
		this.name = Optional.of(name);
		return this;
	}

	public RuleBuilder map(Function<String, Object> typeMapper) {
		this.typeMapper = Optional.of(typeMapper);
		return this;
	}

	public RuleBuilder saveAs(ResultHandler resultHandler) {
		this.resultHandler = Optional.of(resultHandler);
		return this;
	}

	public Rule build() {
		return new Rule(name, typeMapper, resultHandler);
	}
}
