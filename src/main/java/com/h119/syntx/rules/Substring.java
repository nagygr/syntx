package com.h119.syntx.rules;

import lombok.Getter;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static com.h119.syntx.util.string.StringUtil.*;

public class Substring extends BaseRule {
	@Getter
	private String word;

	public Substring(String word) {
		this.word = word;
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int index = context.getPosition();

		if (index >= text.length()) {
			return reportError(errorHandler, index);
		}

		if (startsWithFromIndex(text, index, word)) {
			int start = context.getPosition();
			int end = start + word.length();

			result.setFrom(start);
			result.setTo(end);
			context.setPosition(end);

			return true;
		}

		return reportError(errorHandler, index);
	}

	@Override
	public String toString() {
		return
			String.format(
				"a substring (\"%s\")",
				word
			);
	}
}


