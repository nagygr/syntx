package com.h119.syntx.rules;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static java.lang.Character.isWhitespace;

public class Whitespace extends BaseRule {
	private BaseRule element;

	public Whitespace(BaseRule element) {
		super();
		this.element = element;
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int length = text.length();
		int index = context.getPosition();

		Context localContext = new Context(context);
		Result localResult= new Result(result);

		for (;index < length && isWhitespace(text.charAt(index)); ++index);

		localContext.setPosition(index);

		if (element.match(localContext, localResult, node, errorHandler)) {
			// NOTE: whitespaces are swallowed here
			result.set(localResult);
			context.set(localContext);

			return true;
		}

		return false;
	}
}

