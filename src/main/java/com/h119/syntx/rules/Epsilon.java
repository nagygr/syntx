package com.h119.syntx.rules;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;

public class Epsilon extends BaseRule {
	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		int index = context.getPosition();

		result.setFrom(index);
		result.setTo(index);

		return true;
	}
}

