package com.h119.syntx.rules;

import lombok.Getter;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

public class StringLiteral extends BaseRule {
	@Getter
	private char delimiter;

	@Getter
	private char escape;

	public StringLiteral(char delimiter, char escape) {
		this.delimiter = delimiter;
		this.escape = escape;
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int index = context.getPosition();

		if (index >= text.length()) {
			return reportError(errorHandler, index);
		}

		if (text.charAt(index) == delimiter) {
			boolean endReached = false;
			boolean skip = false;
			index += 1;

			while (index < text.length() && !endReached) {
				if (text.charAt(index) == escape) {
					skip = true;
				}
				else if (!skip && text.charAt(index) == delimiter) {
					endReached = true;
				}
				else {
					skip = false;
				}

				index += 1;
			}

			if (!endReached)
				return reportError(errorHandler, index);

			result.setFrom(context.getPosition());
			result.setTo(index);
			context.setPosition(index);

			return true;
		}

		return reportError(errorHandler, index);
	}

	@Override
	public String toString() {
		return
			String.format(
				"a string literal (delimiter: '%c', escape character: '%c')",
				delimiter, escape
			);
	}
}



