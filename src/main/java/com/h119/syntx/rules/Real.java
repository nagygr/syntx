package com.h119.syntx.rules;

import lombok.Getter;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static com.h119.syntx.util.string.StringUtil.*;

public class Real extends BaseRule {
	@Getter
	private char decimalMark;

	public Real(char decimalMark) {
		this.decimalMark = decimalMark;
	}

	@Override
	@SuppressWarnings("java:S3776") // Cognitive Complexity of methods should not be too high
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int length = text.length();
		int index = context.getPosition();

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		boolean hasIntegerPart = false;
		boolean hasFractionalPart = false;

		if (isCharacterInString(text.charAt(index), "+-")) {
			index += 1;
		}

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		if (text.charAt(index) == '0') {
			hasIntegerPart = true;
			index += 1;
		}
		else if (isCharacterInRange(text.charAt(index), '1', '9')) { // Gather the integer part
			hasIntegerPart = true;
			index += 1;

			while (index < length && isCharacterInRange(text.charAt(index), '0', '9')) {
				index += 1;
			}
		}

		if (index < length && text.charAt(index) == decimalMark) { // Gather the fractional part
			index += 1;

			while (index < length && isCharacterInRange(text.charAt(index), '0', '9')) {
				hasFractionalPart = true;
				index += 1;
			}
		}

		if (index < length && isCharacterInString(text.charAt(index), "eE")) { // Gather the exponential part
			if (!(hasIntegerPart || hasFractionalPart)) return reportError(errorHandler, index);

			index += 1;

			if (index < length && isCharacterInString(text.charAt(index), "+-")) {
				index += 1;
			}

			if (index >= length || !isCharacterInRange(text.charAt(index), '0', '9')) {
				return reportError(errorHandler, index);
			}

			while (index < length && isCharacterInRange(text.charAt(index), '0', '9')) {
				index += 1;
			}
		}

		if (hasIntegerPart || hasFractionalPart) {
			result.setFrom(context.getPosition());
			result.setTo(index);
			context.setPosition(index);

			return true;
		}

		return reportError(errorHandler, index);
	}

	@Override
	public String toString() {
		return
			String.format(
				"a real (decimal mark: '%c')",
				decimalMark
			);
	}
}
