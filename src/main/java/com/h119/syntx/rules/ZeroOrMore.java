package com.h119.syntx.rules;

import lombok.AllArgsConstructor;
import lombok.Value;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;

@Value @AllArgsConstructor
public class ZeroOrMore extends BaseRule {
	private BaseRule element;

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Context localContext = new Context(context);
		Result localResult = new Result(result);

		boolean matched = false;

		while (element.match(localContext, localResult, node, errorHandler)) {
			matched = true;
		}

		if (matched) {
			result.setFrom(context.getPosition());
			result.setTo(localResult.getTo());
			context.set(localContext);
		}
		else {
			result.setFrom(context.getPosition());
			result.setTo(context.getPosition());
		}

		return true;
	}
}
