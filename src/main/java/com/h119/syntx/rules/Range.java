package com.h119.syntx.rules;

import lombok.Getter;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static com.h119.syntx.util.string.StringUtil.*;

public class Range extends BaseRule {
	@Getter
	private char lowerBound;

	@Getter
	private char upperBound;

	public Range(char lowerBound, char upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int index = context.getPosition();

		if (index >= text.length()) {
			return reportError(errorHandler, index);
		}

		if (isCharacterInRange(text.charAt(index), lowerBound, upperBound)) {
			result.setFrom(index);
			result.setTo(index + 1);
			context.setPosition(index + 1);

			return true;
		}

		return reportError(errorHandler, index);
	}

	@Override
	public String toString() {
		return
			String.format(
				"a character in the range: ['%c', '%c']",
				lowerBound, upperBound
			);
	}
}


