package com.h119.syntx.rules;

import lombok.AllArgsConstructor;
import lombok.Value;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;

@Value @AllArgsConstructor
public class Alternation extends BaseRule {
	private BaseRule lhs;
	private BaseRule rhs;

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Context localContext = new Context(context);
		Result lhsResult = new Result(result);
		Result rhsResult = new Result(result);

		if (lhs.match(localContext, lhsResult, node, errorHandler)) {
			result.setFrom(lhsResult.getFrom());
			result.setTo(lhsResult.getTo());
			context.setPosition(localContext.getPosition());

			return true;
		}

		if (rhs.match(localContext, rhsResult, node, errorHandler)) {
			result.setFrom(rhsResult.getFrom());
			result.setTo(rhsResult.getTo());
			context.setPosition(localContext.getPosition());

			return true;
		}

		return false;
	}
}


