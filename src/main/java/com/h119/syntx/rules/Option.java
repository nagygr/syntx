package com.h119.syntx.rules;

import lombok.AllArgsConstructor;
import lombok.Value;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;

@Value @AllArgsConstructor
public class Option extends BaseRule {
	private BaseRule element;

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Context localContext = new Context(context);
		Result localResult = new Result(result);

		if (element.match(localContext, localResult, node, errorHandler)) {
			result.setFrom(localResult.getFrom());
			result.setTo(localResult.getTo());
			context.setPosition(localContext.getPosition());
		}
		else {
			result.setFrom(localContext.getPosition());
			result.setTo(localContext.getPosition());
		}

		return true;
	}
}



