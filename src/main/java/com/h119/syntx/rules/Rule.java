package com.h119.syntx.rules;

import java.util.Optional;
import java.util.function.Function;

import lombok.Getter;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.deserial.ResultHandler;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;

public class Rule extends BaseRule {
	private Optional<BaseRule> innerRule;

	@Getter
	private Optional<String> name;

	@Getter
	private Optional<Function<String, Object>> typeMapper; /* Function that turns the parsed String into another type (or just creates an object when a rule is matched. */

	@Getter
	private Optional<ResultHandler> resultHandler;

	Rule(Optional<String> name, Optional<Function<String, Object>> typeMapper, Optional<ResultHandler> resultHandler) {
		this.name = name;
		this.typeMapper = typeMapper;
		this.resultHandler = resultHandler;
		this.innerRule = Optional.empty();
	}

	public Rule() {
		this(Optional.empty(), Optional.empty(), Optional.empty());
	}

	public Rule set(BaseRule innerRule) {
		this.innerRule = Optional.of(innerRule);
		return this;
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		if (innerRule.isEmpty())
			throw new IllegalStateException(
				String.format(
					"The inner rule of %s rule has not been set.",
					name.map(n -> String.format("\"%s\"", n)).orElse("an unnamed")
				)
			);

		Context localContext = new Context(context);
		Result localResult = new Result(result);
		AstNode localNode = name.isPresent() ? new AstNode(name.get()) : node;

		if (innerRule.get().match(localContext, localResult, localNode, errorHandler)) {
			result.setFrom(localResult.getFrom());
			result.setTo(localResult.getTo());

			context.setPosition(localContext.getPosition());

			if (name.isPresent()) {
				localNode.setResult(localResult);
				localNode.setTypeMapper(typeMapper);
				localNode.setResultHandler(resultHandler);
				node.getChildren().add(localNode);
			}

			return true;
		}

		if (errorHandler.hasError() && name.isPresent()) {
			errorHandler
				.getError()
				.setRuleName(Optional.of(name.get()));
		}

		return false;
	}
}
