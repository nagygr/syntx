package com.h119.syntx.rules;

import java.util.stream.Collectors;

import lombok.Getter;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import static com.h119.syntx.util.string.StringUtil.*;

public class NotCharacter extends BaseRule {
	@Getter
	private String characterSet;

	public NotCharacter(String characterSet) {
		this.characterSet = characterSet;
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int index = context.getPosition();

		if (index >= text.length()) {
			return reportError(errorHandler, index);
		}

		if (!isCharacterInString(text.charAt(index), characterSet)) {
			result.setFrom(index);
			result.setTo(index + 1);
			context.setPosition(index + 1);

			return true;
		}

		return reportError(errorHandler, index);
	}

	@Override
	public String toString() {
		return
			String.format(
				"a character not from the set: {%s}",
				characterSet
					.chars()
					.mapToObj(
						c ->
							String.format(
								"'%c'",
								(char)c
							)
					)
					.collect(Collectors.joining(", "))
			);
	}
}

