package com.h119.syntx.rules;

import java.util.stream.Collectors;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;
import com.h119.syntx.util.text.Parsable;

import lombok.NonNull;

import static com.h119.syntx.util.string.StringUtil.*;

public class Identifier extends BaseRule {
	private String extraCharacters;

	public Identifier(@NonNull String extraCharacters) {
		super();
		this.extraCharacters = extraCharacters;
	}

	public Identifier() {
		this("");
	}

	@Override
	public String toString() {
		String name = "an identifier";
		return
			extraCharacters.equals("")
			? name
			:
				String.format(
					"%s with extra characters: {%s}",
					name,
					extraCharacters
						.chars()
						.mapToObj(
							c ->
								String.format(
									"'%c'",
									(char)c
								)
						)
						.collect(Collectors.joining(", "))
				);
	}

	@Override
	protected boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		Parsable text = context.getText();
		int length = text.length();
		int index = context.getPosition();

		if (index >= length) {
			return reportError(errorHandler, index);
		}

		char current = text.charAt(index);

		if (
			isCharacterInRange(current, 'a', 'z') ||
			isCharacterInRange(current, 'A', 'Z') ||
			isCharacterInString(current, extraCharacters)
		) {
			index += 1;
			current = text.charAt(index);

			while (
				index < length && (
					isCharacterInRange(current, 'a', 'z') ||
					isCharacterInRange(current, 'A', 'Z') ||
					isCharacterInRange(current, '0', '9') ||
					isCharacterInString(current, extraCharacters)
				)
			) {
				index += 1;
				if (index < length) current = text.charAt(index);
			}

			result.setFrom(context.getPosition());
			result.setTo(index);
			context.setPosition(index);

			return true;
		}

		return reportError(errorHandler, index);
	}
}

