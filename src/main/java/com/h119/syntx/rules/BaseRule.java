package com.h119.syntx.rules;

import com.h119.syntx.util.ast.AstNode;
import com.h119.syntx.util.error.ParseError;
import com.h119.syntx.util.error.ParseErrorHandler;
import com.h119.syntx.util.parsing.Context;
import com.h119.syntx.util.parsing.Result;

public abstract class BaseRule {
	public final boolean match(Context context, Result result, AstNode node, ParseErrorHandler errorHandler) {
		return test(context, result, node, errorHandler);
	}

	public BaseRule cat(BaseRule lhs) {
		return new Concatenation(this, lhs);
	}

	public BaseRule or(BaseRule lhs) {
		return new Alternation(this, lhs);
	}

	protected boolean reportError(ParseErrorHandler errorHandler, int index) {
		errorHandler.setError(new ParseError(index, toString()));
		return false;
	}

	protected abstract boolean test(Context context, Result result, AstNode node, ParseErrorHandler errorHandler);
}
