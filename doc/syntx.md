# Introduction

Syntx is a parsing framework that provides a Java DSL for the definition
of grammar rules and their mapping to Java objects.

## Features

Syntx provides a very consize, declarative way of defining grammars within
the Java code. The result of the parsing operation is an AST (Abstract
Syntax Tree) or a detailed error message (in case there was a mismatch
between the given text and the grammar). The syntax tree can then be
mapped to Java classes based on information given at rule construction.

```java
public class Hello {
	public static void main(String... args) {
		System.out.println("Hello world!\n");
	}
}
```

