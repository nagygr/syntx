\LoadClass[10pt]{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{honeonenine}[2021/10/04 h119 doc style]

\RequirePackage
[
	paper=a4paper, top=4cm, left=2cm, right=2cm, bottom=3cm, headheight=100pt, footskip=40pt
]{geometry}

\RequirePackage{fancyhdr}
\RequirePackage[utf8]{inputenc}
\RequirePackage{graphicx}
\RequirePackage{hyperref}
\RequirePackage{caption}
\RequirePackage[T1]{fontenc}
\RequirePackage{listings}
\RequirePackage{bold-extra}
\RequirePackage[usenames,dvipsnames]{color}
\RequirePackage{enumitem}
\RequirePackage{amssymb}
\RequirePackage[breakable]{tcolorbox}

\RequirePackage{tikz}
\usetikzlibrary{shapes.geometric, arrows}

\definecolor{hwlightblue}{HTML}{0055d4}
\definecolor{hwverydarkgray}{HTML}{333333}
\definecolor{hwdarkgray}{HTML}{838e94}
\definecolor{hwlightgray}{HTML}{aaaaaa}
\definecolor{hwverylightgray}{HTML}{f0f0f0}
\definecolor{hwbluetext}{HTML}{3771c8}

\captionsetup[figure]{labelfont={bf,color=hwlightblue},textfont={color=hwverydarkgray}}

\color{hwverydarkgray}

\hypersetup
{
	pdfborder={0 0 0},
	colorlinks=true,
	breaklinks=true,
	urlcolor=hwlightblue,
	linkcolor=hwverydarkgray
}

\lstset
{
	basicstyle=\footnotesize\sffamily\color{hwverydarkgray},
	keywordstyle=\bfseries\color{hwlightblue},aboveskip=2ex,
	commentstyle=\color{hwdarkgray},
	belowskip=2ex,
	tabsize=2,
	xleftmargin=2em,
	captionpos=b,
	breaklines=true,
	language=C++
}

\setlist{noitemsep}
\setlist{topsep=2ex}
\setlist[enumerate,1]
{
	label=\colorbox{hwlightblue}{\textcolor{white}{\footnotesize\arabic*}},
	labelsep=1em,
	leftmargin=3em,
	rightmargin=2em
}
\setlist[itemize,1]
{
	label=\raisebox{0.3ex}{\textcolor{hwlightblue}{\tiny$\blacksquare$}},
	labelsep=1em,
	leftmargin=3em,
	rightmargin=2em
}

\newcommand{\hwquote}[1]{``#1''}

\pagestyle{fancy}

\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancyhf[FC]{
	\begin{tikzpicture}[remember picture, overlay]
		\node(pagearea) [anchor=south west,inner sep=0pt,outer sep=0pt]  at (current page.south west)
		{
			\colorbox{hwverydarkgray}
			{
				\begin{minipage}[h]{\paperwidth}
					\vspace{1.6ex}
					\hfill
					\scriptsize{\textcolor{hwbluetext}{\textbf{\thepage}}}
					\hfill
					\vspace{2ex}
				\end{minipage}
			}
		};
		\node(line)
		[
			rectangle,
			above of=pagearea,
			fill=hwbluetext,
			minimum width=1.1\paperwidth,
			minimum height=0.6ex,
			inner sep=0ex,
			node distance=2.8ex
		] {};
	\end{tikzpicture}
}

\newcommand{\hwtitle}{}
\newcommand{\hwsubtitle}{}

\newcommand{\hwsettitle}[1]{\renewcommand{\hwtitle}{#1\hwsectionheader{#1}}}
\newcommand{\hwsetsubtitle}[1]{\renewcommand{\hwsubtitle}{#1}}

\fancypagestyle{hwcoverpagestyle}{
	\fancyhf{}
}

\newcommand{\hwcoverpage}
{
	\thispagestyle{hwcoverpagestyle}
	\pagecolor{hwverydarkgray}
	\begin{tikzpicture}[remember picture, overlay]
		\node[anchor=north west] at (current page.north west)
		{
			\begin{minipage}[h]{\paperwidth}
				\vspace*{53ex}
				\centerline
				{
					\makeatletter
					\fontsize{1.6cm}{1.8cm}\textbf{\textcolor{hwbluetext}{~\hwtitle}}
					\makeatother
				}
				\vspace{1ex}
				\centerline{\Large\textcolor{hwlightgray}{\hwsubtitle}}
			\end{minipage}
		};
		\node [draw=none,circle,minimum size=6ex,fill=hwbluetext,anchor=south,outer sep=5ex]  at (current page.south)
		{
			\textcolor{hwverylightgray}{\textbf{\textit{h119}}}
		};
	\end{tikzpicture}
	\newpage
	\pagecolor{white}
	\setcounter{page}{1}
}

\newcommand{\hwsectionheader}[1]
{
	\fancyhf[HC]
	{
		\begin{tikzpicture}[remember picture, overlay]
			\node [anchor=north west,inner sep=0ex,outer sep=0ex]  at (current page.north west)
			{
				\colorbox{hwverydarkgray}
				{
					\begin{minipage}[h]{\paperwidth}
						\vspace*{2ex}
						\hspace{12ex}
						\raisebox{2.2ex}
						{
							\large{\textcolor{hwbluetext}{\textbf{\hwtitle}}}
						}
						\hfill
						\raisebox{2.2ex}
						{
							\footnotesize{\textcolor{hwlightgray}{\textbf{#1}}}
						}
						\hspace{4em}
					\end{minipage}
				}
			};
			\node [minimum height=2ex] (topspace) at (current page.north west) {};
			\node [minimum width=2ex,below of=topspace] (leftspace) {};
			\node [draw=none,circle,fill=hwbluetext,minimum size=3ex,anchor=west,right of=leftspace,outer sep=5ex]
			{\textcolor{hwverylightgray}{\footnotesize{\textbf{\textit{h119}}}}};
		\end{tikzpicture}
	}
}

\let\oldsection\section
\renewcommand{\section}[1]
{
	\clearpage
	\newpage
	\oldsection*{\textcolor{hwlightblue}{#1}}
	\label{sec:#1}
	\addcontentsline{toc}{section}{#1}
	\hwsectionheader{#1}
}

\let\oldsubsection\subsection
\renewcommand{\subsection}[1]
{
	\oldsubsection*{\textcolor{hwverydarkgray}{#1}}
	\label{subsec:#1}
	\addcontentsline{toc}{subsection}{#1}
}
